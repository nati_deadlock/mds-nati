package com.example;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyDataStructure {
	private final static int QUEUE_LOAD_FACTOR = 2;
	
	/*
	 * Using ReentrantLock for keep the put/remove operations to be performed
	 * at the same order.
	 */
	private final Lock lock = new ReentrantLock(true);
	
	private final int capacity;
	private Map<String, Element> elementsMap;
	private Map<String, ScheduledExecutorService> removeElementsTasks;
	private Queue<Element> elementsQueue;
	
	
	public MyDataStructure(int capacity) {
		if(capacity<1) {
			System.out.println("MDS Capacity must be at least more than 1");
			capacity=1;
		}
		this.capacity = capacity;
		elementsMap = new HashMap<String, Element>();
		removeElementsTasks = new HashMap<String, ScheduledExecutorService>();
		elementsQueue = new LinkedList<Element>();
	}
	
	/**
	 * add value to the data structure. complexity is o(1)
	 * @param key
	 * @param value
	 * @param timeToLive
	 */
	public void put(String key, Object value, int timeToLive) {
		lock.lock();
			if(elementsMap.containsKey(key)) {
				remove(key);
			}
			Element newElement = new Element(key, value);
			//System.out.println("Adding new element: "+newElement);
			elementsQueue.add(newElement);
			elementsMap.put(key, newElement);
			scheduleRemoveElementTask(key, timeToLive);
			
			if(elementsMap.size()>capacity) {
				removeOldestElement();
			}
			tryRemoveFirstElementFromQueue();
			if(elementsQueue.size()>capacity*QUEUE_LOAD_FACTOR) {
				cleanNotExistedElementsOnMapFromQueue();
			}
		lock.unlock();
	}

	/**
	 * remove value from the data structure. complexity is o(1)
	 * @param key
	 */
	public void remove(String key) {
		lock.lock();
			Element element = elementsMap.remove(key);
			if(element!=null) {
				element.existsOnMap = false;
				//System.out.println("Element removed from map: "+element.toString());
			}
			
			ScheduledExecutorService scheduledExecutorService = removeElementsTasks.remove(key);
			if(scheduledExecutorService!=null) {
				//System.out.println("shutting down task of id: "+key);
				scheduledExecutorService.shutdownNow();
			}
			
		lock.unlock();
	}

	/**
	 * get value from the data structure. complexity is o(1)
	 * @param key
	 * @return
	 */
	public Object get(String key) {
		lock.lock();
			Object result = new Object();
			Element element = elementsMap.get(key);
			if(element==null) {
				result=null;
			}else {
				result = element.value;
			}
		lock.unlock();
		return result;
	}

	/**
	 * get number of keys in the data structure. complexity is o(1)
	 * @return number of keys in the data structure
	 */
	public int size() {
		lock.lock();
			int size = elementsMap.size();
		lock.unlock();
		return size;
	}
	
	private void tryRemoveFirstElementFromQueue() {
		Element firstElement = elementsQueue.peek();
		if(firstElement==null) {
			return;
		}
		
		if(!firstElement.existsOnMap) {
			elementsQueue.remove();
		}
	}
	
	private void removeOldestElement() {
		Boolean elementRemoved = false;
		while(!elementRemoved) {
			Element elementFromQueue = elementsQueue.remove();
			if(elementFromQueue.existsOnMap) {
				remove(elementFromQueue.key);
				elementRemoved = true;
			}
		}
	}
	
	/*
	 * Queue is cleaned for avoiding memory leak.
	 * Invoked only when queueSize > capacity*2 (rare scenario).
	 */
	private void cleanNotExistedElementsOnMapFromQueue() {
		Queue<Element> newElementsQueue = new LinkedList<Element>();
		for(Element element : this.elementsQueue) {
			if(element.existsOnMap) {
				newElementsQueue.add(element);
			}
		}
		this.elementsQueue = newElementsQueue;
	}
	
	private void scheduleRemoveElementTask(String key, int timeToLive) {
		if(timeToLive<=0) {
			return;
		}
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
		Runnable removeElementTask = new Runnable() {
			@Override
			public void run() {
				//System.out.println("Running scheduled remove task, key: "+key);
				remove(key);
			}
		};
		scheduler.schedule(removeElementTask, timeToLive, TimeUnit.MILLISECONDS);
		removeElementsTasks.put(key, scheduler);
	}
	
	private static class Element{
		String key;
		Object value;
		Boolean existsOnMap;
		
		public Element(String key, Object value) {
			this.key = key;
			this.value = value;
			existsOnMap = true;
		}

		@Override
		public String toString() {
			return "Element [key=" + key + ", value=" + value + ", existsOnMap="
					+ existsOnMap + "]";
		}
	}
}
